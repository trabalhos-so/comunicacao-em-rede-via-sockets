SRC_LOJA=$(shell find ./src/loja/ -name "*.cpp" -type f)
SRC_CLIENTE=$(shell find ./src/cliente/ -name "*.cpp" -type f)
$(shell mkdir -p objects/loja)
$(shell mkdir -p objects/cliente)
OBJ_LOJA=$(patsubst ./src/loja%, ./objects/loja%, $(patsubst %.cpp, %.o , $(SRC_LOJA)))
OBJ_LOJA+=./objects/socket.o
OBJ_CLIENTE=$(patsubst ./src/cliente%, ./objects/cliente%, $(patsubst %.cpp, %.o , $(SRC_CLIENTE)))
OBJ_CLIENTE+=./objects/socket.o
FLAGS=-Wall -Wextra -Wshadow -Werror -I include -I include/loja -I include/cliente

loja: $(OBJ_LOJA)
	g++ -o $@ $^ $(FLAGS)

cliente: $(OBJ_CLIENTE)
	g++ -o $@ $^ $(FLAGS)

objects/%.o: src/%.cpp
	g++ -c -o $@ $^ $(FLAGS)

clean:
	@find . -type f -name "*.o" -exec rm '{}' \;
	@find . -type f -name "loja" -exec rm '{}' \;
	@find . -type f -name "cliente" -exec rm '{}' \;
	@rmdir objects/loja objects/cliente objects;

clean_loja:
	@find ./objects/loja/ -type f -name "*.o" -exec rm '{}' \;
	@find . -type f -name "loja" -exec rm '{}' \;
	@rmdir objects/loja

clean_cliente:
	@find ./objects/cliente/ -type f -name "*.o" -exec rm '{}' \;
	@find . -type f -name "cliente" -exec rm '{}' \;
	@rmdir objects/cliente

valgrind_l:
	$(MAKE) loja
	valgrind --leak-check=full --show-leak-kinds=all ./loja 8080 15 15 15

valgrind_c:
	$(MAKE) cliente
	valgrind --leak-check=full --show-leak-kinds=all ./cliente 127.0.0.1 8080

tl:
	$(MAKE) loja
	./loja 8080 5 5 5
	
tc:
	$(MAKE) cliente
	./cliente 127.0.0.1 8080