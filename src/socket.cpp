#include "socket.hpp"

Socket::Socket()
{
}

Socket::~Socket()
{
}

int Socket::Start()
{
    return socket(AF_INET, SOCK_STREAM, 0);
}

void Socket::End()
{

}

void Socket::Send(int _remetente, char* _mensagem)
{
    send(_remetente, _mensagem, std::char_traits<char>::length(_mensagem), 0);
}

void Socket::Receive(int _origem, std::string& _mensagem)
{
    char buffer[1024] = {0};
    read(_origem, buffer, 1024);
    _mensagem = buffer;
}
