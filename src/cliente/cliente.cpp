#include "client.hpp"
#include <iostream>
#include <string>


int main(int argc, char* argv[])
{
    std::string separador = "----------------------------------\n";
    Client Cliente;
    Cliente.Setup(Cliente.verificaArgumentos(argc, argv), argv[1]);
    std::string s = "Ola!";
    Cliente.Message(s);
    std::string mensagem;
    Cliente.Message(mensagem);
    std::cout << separador << mensagem << "";
    while(mensagem.find("Realizado!") == std::string::npos)
    {
        std::getline(std::cin >> std::ws, mensagem);
        Cliente.Message(mensagem);
        mensagem.clear();
        Cliente.Message(mensagem);
        std::cout << separador << mensagem << "";
    }
    std::cout << separador;
    return 0;
}