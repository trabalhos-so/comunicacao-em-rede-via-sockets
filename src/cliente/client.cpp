#include "client.hpp"

Client::Client()
{
    port = 0;
    ip = nullptr;
    socketClient = 0;
}

Client::~Client()
{

}

void Client::Setup(int _port, char* _ip)
{
    port = _port;
    ip = _ip;
    socketClient = Socket::Start();
    if(socketClient == CODIGO_ERRO)
    {
        std::cerr << "Erro ao criar o socket!\n";
        exit(1);
    }
    Client::Config();
    Client::Connect();
}

void Client::Config()
{
    enderecoServidor.sin_family = AF_INET;
    enderecoServidor.sin_port = htons(port);

    if(inet_pton(AF_INET, ip, &enderecoServidor.sin_addr) <= 0) 
    {
        std::cerr << "IP invalido! (" << ip << ")\n";
        exit(1);
    }
}

void Client::Connect()
{
    if (connect(socketClient, (struct sockaddr *)&enderecoServidor, sizeof(enderecoServidor)) == CODIGO_ERRO)
    {
        std::cerr << "Erro ao conectar o cliente ao IP (" << ip << ":" << port << ")!\n";
        exit(1);
    }
}

void Client::Message(std::string& _mensagem)
{
    if(_mensagem.empty() == true) Socket::Receive(socketClient, _mensagem);
    else
    {
        char* msg = &_mensagem[0];
        Socket::Send(socketClient, msg);
    }
}

int Client::verificaArgumentos(int _argc, char** _argv)
{
    int verificaPort = -1;
    if(_argc != 3)
    {
        std::cerr << "Execute o programa conforme instrucoes do arquivo \"README.md\".\n";
        exit(1);
    }
    try
    {
        verificaPort = std::stoi(_argv[2], nullptr);
        if(verificaPort < 0)
        {
            std::cerr << "Valor invalido passado como argumento!" << std::endl;
            exit(1);
        }
    } catch(std::invalid_argument& e) {
        std::cerr << "Valor invalido passado como argumento!" << std::endl;
        std::cerr << "Execute o programa conforme instrucoes do arquivo \"README.md\".\n";
        exit(1);
    }
    return verificaPort;
}