#include "server.hpp"
#include "atendente.hpp"

int main(int argc, char* argv[])
{
    Server Loja;
    Atendente atendenteObj;
    int port = atendenteObj.InterpretaArgumentos(argc, argv);
    Loja.Setup(port);
    int socketCliente = -1;
    if(atendenteObj.ContagemEstoque() > 0) std::cout << "Loja aberta!\n";
    while(atendenteObj.ContagemEstoque() > 0)
    {
        Loja.Listen();
        socketCliente = Loja.Accept();
        std::string mensagem;
        while(mensagem.find("Realizado!") == std::string::npos)
        {
            mensagem.clear();
            Loja.Message(socketCliente, mensagem);
            std::cout << "Cliente disse: " << mensagem << std::endl;
            atendenteObj.InterpretaPedido(mensagem);
            Loja.Message(socketCliente, mensagem);
        }
    }
    std::cout << "Loja fechando!\n";
    return 0;
}