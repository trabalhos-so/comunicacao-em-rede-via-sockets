#include "atendente.hpp"

Atendente::Atendente()
{
    estoqueBebidas = {0, 0, 0};
}

Atendente::~Atendente()
{}

int Atendente::ContagemEstoque()
{
    int contagem = 0;
    for(unsigned int i = 0; i < estoqueBebidas.size(); i++) contagem += estoqueBebidas[i];
    return contagem;
}

void Atendente::InterpretaPedido(std::string& _pedido)
{
    std::string mensagemErro = "Ocorreu um erro com a entrada! Digite \"menu\" para ver o cardapio e disponibilidade.\nTente novamente: ";
    std::string indisponibilidade = "Infelizmente nao temos em estoque essa quantidade.\nTente uma valor ou bebida diferente se desejar: ";
    int drink = 0, unidades = -1;
    if(_pedido == "Ola!") _pedido = mostraCardapio(true);
    else if(_pedido == "menu") _pedido = mostraCardapio(false);
    else 
    {
        std::string bebidaPedida;
        bebidaPedida.assign(_pedido, _pedido.find(" ") + 1 , _pedido.size() - _pedido.find(" "));
        if(bebidaPedida.size() != 1)
        {
            _pedido = mensagemErro;
            return;
        }
        switch (bebidaPedida[0])
        {
            case 'C':
                drink = 0;
                break;
            case 'A':
                drink = 1;
                break;
            case 'R':
                drink = 2;
                break;
            case 'c':
                drink = 0;
                break;
            case 'a':
                drink = 1;
                break;
            case 'r':
                drink = 2;
                break;
            default:
                _pedido = mensagemErro;
                break;
        }
        try
        {
            unidades = std::stoi(_pedido, nullptr);
            if(unidades <= 0)
            {
                _pedido = mensagemErro;
                return;
            } else if(estoqueBebidas[drink] - unidades < 0){
                _pedido = indisponibilidade;
                return;
            } else {
                _pedido = "\033[0;35mPedido Realizado!\033[0m\n";
                estoqueBebidas[drink] -= unidades;
                if(ContagemEstoque() == 0) _pedido.append("Obrigado por comprar, os estoques acabaram!\n");
            }
        } catch(std::invalid_argument& e) {
            _pedido = mensagemErro;
            return;
        }
    }
}

int Atendente::InterpretaArgumentos(int _qtdArgs, char** _arg)
{
    if(_qtdArgs != 5)
    {
        std::cerr << "Execute o programa conforme instrucoes do arquivo \"README.md\".\n";
        exit(1);
    }
    int port = 0;
    int contadorArg = 1;
    while(contadorArg < 5)
    {
        try
        {
            int leituraArg = std::stoi(_arg[contadorArg], nullptr);
            if(leituraArg >= 0)
            {
                if(contadorArg == 1) port = leituraArg;
                else estoqueBebidas[contadorArg - 2] = leituraArg;
            } else {
                std::cerr << "Valor invalido passado como argumento!" << std::endl;
                exit(1);
            }
        } catch(std::invalid_argument& e) {
            std::cerr << "Valor invalido passado como argumento!" << std::endl;
            std::cerr << "Execute o programa conforme instrucoes do arquivo \"README.md\".\n";
            exit(1);
        }
        contadorArg++;
    }
    return port;
}

std::string Atendente::mostraCardapio(bool saudacaoCompleta)
{
    std::string cardapioQuantidades;
    std::vector<std::string> cardapioBebidas = {"\033[1;33mCerveja\033[0m", "\033[1;34mAgua\033[0m", "\033[0;31mRefrigerante\033[0m"};
    if(saudacaoCompleta == true) cardapioQuantidades.append("\033[0;32mOla cliente, que produto de nossa loja voce deseja?\nSegue nossa carta de bebidas:\033[0m");
    for(unsigned int i = 0; i < 3; i++)
    {
        std::string temporario;
        temporario.append("\n=>");
        temporario.append(cardapioBebidas[i]);
        if(i == 1) temporario.append("\t");
        if(estoqueBebidas[i] == 0) temporario.append("\033[0;31m");
        temporario.append("\t(");
        temporario.append(std::to_string(estoqueBebidas[i]));
        temporario.append(" unidades disponiveis)");
        if(estoqueBebidas[i] == 0) temporario.append("\033[0m");
        cardapioQuantidades.append(temporario);
    }
    cardapioQuantidades.append("\n");
    if(saudacaoCompleta == true) cardapioQuantidades.append("\nDigite seu pedido no formato \"QUANTIDADE INICIAL\" (2 cervejas = \"2 C\")");
    cardapioQuantidades.append("\nPedido: ");
    return cardapioQuantidades;
}