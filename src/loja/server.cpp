#include "server.hpp"

Server::Server()
{
}

Server::~Server()
{
}

void Server::Setup(int _port)
{
    port = _port;
    atributoServer = Socket::Start();
    if(atributoServer == CODIGO_ERRO)
    {
        std::cerr << "Erro ao criar o socket!\n";
        exit(1);
    }
    if(Server::Options() == CODIGO_ERRO)
    {
        std::cerr << "Erro ao definir as opcoes do socket!\n";
        exit(1);
    }
    Server::Bind();
}

int Server::Options()
{
    int opt = 1;
    return setsockopt(atributoServer, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt));
}

void Server::Bind()
{
    enderecoSocket.sin_family = AF_INET;
    enderecoSocket.sin_addr.s_addr = INADDR_ANY;
    enderecoSocket.sin_port = htons(port);
    bind(atributoServer, (sockaddr*)&enderecoSocket, sizeof(enderecoSocket));
}

int Server::Listen()
{
    int returnListen = listen(atributoServer, 3);
    if(returnListen == CODIGO_ERRO)
    {
        std::cerr << "Erro ao executar funcao LISTEN na porta " << port << " !\n";
        exit(1);
    }
    return returnListen;
}

int Server::Accept()
{
    int enderecoSocketSize = sizeof(enderecoSocket);
    int returnAccept = accept(atributoServer, (sockaddr *)&enderecoSocket, (socklen_t*)&enderecoSocketSize);
    if(returnAccept == CODIGO_ERRO)
    {
        std::cerr << "Erro ao aceitar a conexao de um cliente na porta " << port << " !\n";
        exit(1);
    }
    return returnAccept;
}

void Server::Message(int _socketCliente, std::string& _mensagem)
{
    if(_mensagem.empty() == true) Socket::Receive(_socketCliente, _mensagem);
    else
    {
        char* msg = &_mensagem[0];
        Socket::Send(_socketCliente, msg);
    }
}