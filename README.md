# Comunicação em Rede via Sockets

Trabalho com objetivo de exercitar a comunicação e troca de informações em rede via sockets.<br>
Neste trabalho, uma loja de bebidas representará o servidor e os clientes de tal loja representaram os clientes do servidor.

# Alunos

* Bernardo Dalfovo de Souza - 18204849
* Vinicius Slovinski        - 18201356

# Como compilar:

* Loja: ```make loja``` sem as aspas no terminal quando estiver na pasta do projeto.

* Cliente: ```make cliente``` sem as aspas no terminal quando estiver na pasta do projeto.

# Como executar:

* Loja: ```./loja PORT nC nA nR``` sem as aspas no terminal quando estiver na pasta do projeto.

    * "PORT": é a porta desejada;
    * "nC": quantidade de cervejas que iniciará o programa;
    * "nA": quantidade de garrafas de água que iniciará o programa;
    * "nR": quantidade de latas de refrigerante que iniciará o programa.

* Cliente: ```./cliente IP PORT``` sem as aspas no terminal quando estiver na pasta do projeto.

    * "IP": é o ip ao qual se deseja conectar;
    * "PORT": é a porta desejada.

# Outros comandos "make":

* "make clean" : Limpa TODOS os arquivos executaveis e objetos;

* "make clean_loja" : Limpa os arquivos executaveis e objetos apenas da parte da LOJA;

* "make clean_cliente" : Limpa os arquivos executaveis e objetos apenas da parte do CLIENTE;

* "make valgrind" : Compila o projeto e roda o valgrind para verificar vazamento de memoria.
