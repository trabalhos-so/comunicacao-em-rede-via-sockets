#ifndef SOCKET_HPP
#define SOCKET_HPP

#include <sys/socket.h>
#include <unistd.h>     //READ
#include <string>
#define CODIGO_ERRO -1

class Socket
{
    public:
        Socket();
        ~Socket();
        int Start();
        void End();
        void Send(int, char*);
        void Receive(int, std::string&);
    private:
};


#endif