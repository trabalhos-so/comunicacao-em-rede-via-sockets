#ifndef ATENDENTE_HPP
#define ATENDENTE_HPP

#include <vector>
#include <string>
#include <iostream>

class Atendente
{
    public:
        Atendente();
        ~Atendente();
        int ContagemEstoque();
        void InterpretaPedido(std::string&);
        int InterpretaArgumentos(int, char**);
        std::string mostraCardapio(bool);
    private:
        std::vector<int> estoqueBebidas;
};

#endif