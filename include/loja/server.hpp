#ifndef SERVER_HPP
#define SERVER_HPP

#include <netinet/in.h>
#include <iostream>
#include <string>
#include "socket.hpp"

class Server : Socket
{
    public:
        Server();
        ~Server();
        void Setup(int);
        int Listen();
        int Accept();
        void Message(int, std::string&);
    private:
        int Options();
        void Bind();

        sockaddr_in enderecoSocket;
        int atributoServer;
        int port;
};


#endif