#ifndef CLIENT_HPP
#define CLIENT_HPP

#include <arpa/inet.h>
#include <iostream>
#include "socket.hpp"

class Client : Socket
{
    public:
        Client();
        ~Client();
        void Setup(int, char*);
        void Message(std::string&);
        int verificaArgumentos(int, char**);
    private:
        void Config();
        void Connect();
        struct sockaddr_in enderecoServidor;
        int socketClient;
        int port;
        char* ip;
};



#endif
